package cn.oge.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TestController {
	
	@RequestMapping(value = "/hello")
	public String helloWorld(Model model) {
		model.addAttribute("message", "你好，小伙!!!");
		return "HelloWorld";
	}

}
